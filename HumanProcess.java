package com.company;

public class HumanProcess {
    public static void main(String[] args){

        Human human = new Human();
        Human  mother = new Human();
        Human father = new Human();
        Pet pet =  new Pet();
        human.setPet(pet);
        //System.out.println(human.petNicknameAndHumanName());

        mother.setName("Jane");
        mother.setSurname("Karleone");
        mother.setYear(1987);
        mother.setIq(88);

        father.setName("Vito");
        father.setSurname("Karleone");
        father.setYear(1986);
        father.setIq(79);

        pet.setSpecies("dog");
        pet.setNickname("Rock");
        pet.setAge(5);
        pet.setTrickLevel(75);
        pet.setHabits(new String[] {"eat", "sleep", "drink"});

        human.setName("Michael");
        human.setSurname("Karleone");
        human.setYear(1977);
        human.setIq(90);
        human.setMother(mother);
        human.setFather(father);
        //human.setPet(pet);

        System.out.println(pet);
        System.out.println(human);


        String petNicknameAndHumanNameAfter = human.petNicknameAndHumanName();
        System.out.println(petNicknameAndHumanNameAfter);


        pet.eat();
        pet.respond();
        pet.foul();
        human.greetPet();
        human.describePet();




    }
}
